output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "public_subnet" {
  value = aws_subnet.public_subnet
}

output "private_subnet" {
  value = aws_subnet.private_subnet
}

output "private_subnet_cidrs" {
  value = aws_subnet.private_subnet.*.cidr_block
}

output "private_subnet_cidr_map" {
  value = { for x in aws_subnet.private_subnet.* : x.id => x.cidr_block }
}

output "public_security_groups" {
  value = aws_security_group.cidr_sg["public"]
}

output "internal_security_groups" {
  value = aws_security_group.cidr_sg["internal"]
}

output "private_security_groups" {
  value = aws_security_group.sg_sg
}
output "cidr_security_groups" {
  value = aws_security_group.cidr_sg
}

