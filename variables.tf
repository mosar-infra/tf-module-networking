# networking/variables.tf
variable "environment" {}
variable "public_subnet_count" {}
variable "private_subnet_count" {}
variable "vpc_cidr" {}
variable "public_cidrs" {}
variable "private_cidrs" {}
variable "max_subnets" {}
variable "cidr_security_groups" {}
variable "sg_security_groups" {}
variable "create_natgw" {}
variable "environment" {}
