# networking/main.tf

resource "random_integer" "random" {
  min = 1000
  max = 9999
}

data "aws_availability_zones" "available" {
  state = "available"
}

resource "random_shuffle" "az_list" {
  input        = data.aws_availability_zones.available.names
  result_count = var.max_subnets
}

resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags = {
    Name = "mosar_vpc-${random_integer.random.id}"
    Environment = var.environment
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_subnet" "public_subnet" {
  count                   = var.public_subnet_count
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.public_cidrs[count.index]
  map_public_ip_on_launch = true
  availability_zone       = random_shuffle.az_list.result[count.index]
  tags = {
    Name = "mosar_public_${count.index + 1}"
    Environment = var.environment
  }
}

resource "aws_subnet" "private_subnet" {
  count                   = var.private_subnet_count
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.private_cidrs[count.index]
  map_public_ip_on_launch = true
  availability_zone       = random_shuffle.az_list.result[count.index]
  tags = {
    Name = "mosar_private_${count.index + 1}"
    Environment = var.environment
  }
}

resource "aws_security_group" "cidr_sg" {
  for_each = var.cidr_security_groups
  name     = each.value.name
  vpc_id   = aws_vpc.vpc.id
  dynamic "ingress" {
    for_each = each.value.ingress
    content {
      from_port   = ingress.value.from
      to_port     = ingress.value.to
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "mosar_${each.value.name}"
    Environment = var.environment
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "sg_sg" {
  for_each = var.sg_security_groups
  name     = each.value.name
  vpc_id   = aws_vpc.vpc.id
  dynamic "ingress" {
    for_each = each.value.ingress
    content {
      from_port       = ingress.value.from
      to_port         = ingress.value.to
      protocol        = ingress.value.protocol
      security_groups = ingress.value.security_groups
    }
  }
  dynamic "egress" {
    for_each = each.value.egress
    content {
      from_port   = egress.value.from
      to_port     = egress.value.to
      protocol    = egress.value.protocol
      cidr_blocks = egress.value.cidr_blocks
    }
  }
  tags = {
    Name = "mosar_${each.value.name}"
    Environment = var.environment
  }
  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "mosar_igw"
    Environment = var.environment
  }
}

resource "aws_eip" "nat_gw_eip" {
  count = var.create_natgw ? 1 : 0
  vpc   = true
  tags = {
    Name = "nat_gw_eip"
    Environment = var.environment
  }
}

resource "aws_nat_gateway" "nat_gw" {
  count         = var.create_natgw ? 1 : 0
  allocation_id = aws_eip.nat_gw_eip[0].id
  subnet_id     = aws_subnet.public_subnet[0].id
  tags = {
    Name = "mosar_nat_gw"
    Environment = var.environment
  }
  depends_on = [aws_internet_gateway.igw]
}

resource "aws_route_table" "private_rt" {
  count  = var.create_natgw ? 1 : 0
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gw[0].id
  }
  tags = {
    Name = "mosar_private_rt_natgw"
    Environment = var.environment
  }
}

resource "aws_route_table_association" "private_natgw_rt_association" {
  count          = var.create_natgw ? var.private_subnet_count : 0
  subnet_id      = aws_subnet.private_subnet.*.id[count.index]
  route_table_id = aws_route_table.private_rt[0].id
}

resource "aws_default_route_table" "default_private_rt" {
  default_route_table_id = aws_vpc.vpc.default_route_table_id
  tags = {
    Name = "mosar_default_private_route_table"
    Environment = var.environment
  }
}

resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "mosar_public_rt"
    Environment = var.environment
  }
}

resource "aws_route" "default_public_route" {
  route_table_id         = aws_route_table.public_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id
}

resource "aws_route_table_association" "public_rt_association" {
  count          = var.public_subnet_count
  subnet_id      = aws_subnet.public_subnet.*.id[count.index]
  route_table_id = aws_route_table.public_rt.id
}
